# Face mask and gender detector

In this repository we are going to develop a model capable of classifying whether a face has a mask on or not, and its respective gender.

Database: https://drive.google.com/drive/folders/1v3Q75UWNZm-1ZlM5K6pLj2OQ4pA8LnHj?usp=sharing

code open source by Matias Valdivia

Mas informacion: https://matiasvaldivia.com/blogs/blogs/detector-de-mascarilla-facial-y-de-genero

More info: https://matiasvaldivia.com/blogs/blogs/face-mask-and-gender-detector
